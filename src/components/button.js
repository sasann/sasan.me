import styled from '@emotion/styled'

export default styled.a`
    display: inline-block;
    margin: 15px 0;
    font-family: 'Fira Sans';
    font-size: 1.08rem;
    font-weight: 500;
    text-align: center;
    text-decoration: none;
    color: white;
    padding: 7px 20px;
    background-color: #6eb5cf;
    border-radius: 20px;
`