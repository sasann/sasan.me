import React from "react"
import { css } from "@emotion/core"
import img from "../assets/avatar.jpg"

export default () => (
  <div
    css={css`
      width: 8rem;
      height: 8rem;
      background-image: url(${img});
      background-position: center;
      background-size: contain;
      border-radius: 50%;
      margin: 2.5rem auto;
    `}
  ></div>
);