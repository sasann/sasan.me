import React from "react"
import { css } from "@emotion/core"
import Button from "./button.js"
export default () => (
    <>
        <p>I've been fascinated by computers from a young age. Now I make a living building things that run on them.</p>
        <p>I use React, JavaScript and Linux in most of things I create.</p>
        <p>Wanna ask me to help build your product or just say hi? Get in touch!</p>
        <Button href="mailto:hello@sasan.me">Contact Me</Button>
    </>
)