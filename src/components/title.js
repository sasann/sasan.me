import React from "react"
import { css } from "@emotion/core"

const titleStyle = css({
    fontWeight: "inherit",
    margin: 0
})

const mainTitleStyle = css(
    titleStyle,
    {
        fontSize: '1.7rem',
        marginBottom: 5
    }
)

export default () => (
    <header css={css`
        text-align: center;
        font-family: 'Fira Sans';
        font-weight: 500;
        margin: 35px 0;
    `}>
        <h1 css={mainTitleStyle}>Sasan Namiranian</h1>
        <h4 css={titleStyle}>Software Engineer</h4>
    </header>
)