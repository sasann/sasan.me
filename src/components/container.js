import React from "react";
import { css } from "@emotion/core"

export default ( { children } ) => (
  <div css={css`
    box-sizing: border-box;
    box-shadow: -4px 4px 21px 4px rgba(0, 0, 0, 0.2);
    text-align: center;
    padding: 15px 25px;
    border-radius: 20px;
    background-color: white;
    margin-right: auto;
    margin-left: auto;
    width: 85%;
    max-width: 540px;
  `}>
    { children }
  </div>
);