import React from "react";
import { Global, css } from "@emotion/core"

export default ({ children }) => (
    <div css={css`
    min-height: 100vh;
    font-family: 'Open Sans', sans-serif;
    background-image: linear-gradient(25deg, hsl(319, 24%, 61%) 50%, rgb(82, 152, 177));
    display: flex;
    flex-direction: column;
    justify-content: center;
  `}>
        {children}
    </div>
);