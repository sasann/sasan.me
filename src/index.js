import React from "react"
import ReactDOM from "react-dom"

import Container from "./components/container.js"
import Global from "./components/global.js"
import Content from "./components/content.js"
import Avatar from "./components/avatar.js"
import Title from "./components/title.js"
import Description from "./components/description.js"

const App = () => (
    <Content>
        <Global />
        <Container>
            <Avatar />
            <Title />
            <Description />
        </Container>
    </Content>
)

ReactDOM.render(<App />, document.getElementById("root"))